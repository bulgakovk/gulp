const webpackConfig = require('./webpack.config.js');
const testAll = require('./gulpfile.js').testAll;

let browsers;

if (testAll()) {
    browsers = ['PhantomJS', 'Opera', 'Chrome'];
} else {
    browsers = ['PhantomJS'];
}

module.exports = function (config) {
  config.set({
    basePath: '',
    frameworks : ['jasmine'],
    files: [
      'test/*.ts',
      'build/bundle.js'
    ],
    exclude: [
    ],
    preprocessors: {
      'test/**/*.ts': ['webpack']
    },
    webpack: {
      module: webpackConfig.module,
      resolve: webpackConfig.resolve
    },
    reporters: ['progress'],
    port: 9876,
    colors: true,
    logLevel: config.LOG_INFO,
    autoWatch: true,
    browsers: browsers,
    singleRun: false,
    concurrency: 1
  })
}