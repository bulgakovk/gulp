'use strict';

const gulp = require('gulp'),
  typescript = require('gulp-typescript'),
  rigger = require('gulp-rigger'),
  less = require('gulp-less'),
  gulpif = require('gulp-if'),
  sourcemaps = require('gulp-sourcemaps'),
  runSequence = require('run-sequence'),
  babel = require('gulp-babel'),
  gtb = require('gulp-typescript-babel'),
  uglify = require('gulp-uglify'),
  clean = require('gulp-clean'),
  browserSync = require("browser-sync"),
  reload = browserSync.reload,
  cssmin = require('gulp-minify-css'),
  Server = require('karma').Server,
  browserify = require('gulp-browserify'),
  mainBowerFiles = require('gulp-main-bower-files'),
  webpack = require('webpack-stream');

const path = {
  src : {
    html : "./html/index.html",
    css  : "./styles/main.less",
    js   : "./scripts/app.ts",
    test : "./test/**/*.ts"
  },

  build : {
    html       : "./build",
    css        : "./build/styles",
    js         : "./",
    self       : "./build",
    components : "./build/components",
    test       : "./buildTest"
  },

  watch : {
    html : "./html/**/*.html",
    css  : "./styles/*.less",
    js   : "./scripts/**/*.ts"
  }
};

/*
Web-server config.
 */
const config = {
  server: {
    baseDir: path.build.self
  },
  host: '127.0.0.1',
  port: 9000
};

let debugMode = false;
let testAll   = false;
module.exports.testAll = function () {
    return testAll;
}; // to check in karma whether make tests in all browsers

/*
Helper tasks.
 */

gulp.task('html:build', () => {
  return gulp.src(path.src.html)
  .pipe(rigger())
  .pipe(gulp.dest(path.build.html))
  .pipe(reload({stream: true}));
});

gulp.task('css:build', () => {
  return gulp.src(path.src.css)
  .pipe(gulpif(!debugMode, sourcemaps.init()))
  .pipe(less())
  .pipe(gulpif(!debugMode || testAll, cssmin()))
  .pipe(gulpif(!debugMode || testAll, sourcemaps.write()))
  .pipe(gulp.dest(path.build.css))
  .pipe(reload({stream: true}));
});

gulp.task('js:build', ['bower'], () => {
  return gulp.src(path.src.js)
  .pipe(gtb({incremental: true, configFile: 'tsconfig.json'},
      { presets: ['es2015'],
        plugins: [ "syntax-async-functions",
                   "transform-es2015-modules-commonjs",
                   "transform-async-to-generator",
                   "transform-regenerator",
                   "transform-runtime"
                  ]}))
  .pipe(browserify())
  .pipe(gulpif(!debugMode, sourcemaps.init()))
  .pipe(gulpif(!debugMode || testAll, uglify()))
  .pipe(gulpif(!debugMode, sourcemaps.write()))
  .pipe(gulp.dest(path.build.self))
  .pipe(reload({stream: true}));

});

gulp.task('test:build', () => {
  return gulp.src(path.src.js)
  .pipe(webpack(require('./webpack.config.js')))
  .pipe(babel({
      presets: ['es2015'],
      plugins: ["transform-runtime",
          "transform-es2015-modules-commonjs"]
  }))
  .pipe(browserify())
  .pipe(gulp.dest(path.build.js))
});

gulp.task('clean', () => {
  return gulp.src(path.build.self, {read : false})
  .pipe(clean());
});

gulp.task('webserver', () => {
  browserSync(config);
});

/*
Bower task should be customized.
*/
gulp.task('bower', () => {
  return gulp.src('./bower.json')
  .pipe(mainBowerFiles())
  .pipe(uglify())
  .pipe(gulp.dest(path.build.components));
});

gulp.task('test:helper', ['test:build'], (done) => {
  new Server({
    configFile : __dirname + '/karma.conf.js',
    singleRun  : true
  }, done).start();
});
/*
  Main tasks below.
*/

gulp.task('build:debug', cb => {
  debugMode = true;
  runSequence('clean', ['html:build', 'css:build', 'js:build', 'bower'], cb);
});

gulp.task('build:release', cb => {
  debugMode = false;
  //FIXME : add minification + d.ts
  runSequence('clean', ['html:build', 'css:build', 'js:build', 'bower'], cb);
});

gulp.task('watch', cb => {
  debugMode = true;
  runSequence('clean', ['build:debug', 'bower'], 'webserver', cb);

  gulp.watch(path.watch.html,['html:build']);
  gulp.watch(path.watch.css, ['css:build']);
  gulp.watch(path.watch.js,  ['js:build']);
});

gulp.task('test', () => {
  testAll = false;

  runSequence('clean', 'test:helper');
});

gulp.task('test:all', () => {
  testAll = true;

  runSequence('clean', 'test:helper');
});
