import Animal from '../scripts/class.ts';

describe('class test', () => {
    it("hello test", () => {
        var animal : Animal = new Animal("Martin");
        expect(animal.sayHello()).toEqual("Hello Martin");

    });
});