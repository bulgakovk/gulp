import * as angular from 'angular';
import 'angular-mocks';

describe('test', () => {
    var $controller;

    beforeEach(angular.mock.module('testApp'));
    beforeEach(angular.mock.inject( (_$controller_) => {
       $controller = _$controller_;
    }));

    it('1+2 should be 3', () => {
        var $scope:any = {};
        var controller = $controller('CalculatorController', { $scope: $scope });
        $scope.x = 1;
        $scope.y = 2;
        $scope.sum();
        expect($scope.z).toEqual(3);
    });

    it('5+5 should be 10', () => {
        var $scope:any = {};
        var controller = $controller('CalculatorController', { $scope: $scope });
        $scope.x = 5;
        $scope.y = 5;
        $scope.sum();
        expect($scope.z).toEqual(10);
    });
});