class Animal {
    constructor (name : string) {
        this.name = name;
    }

    name : string;
    sayHello () {
        return "Hello " + this.name;
    }
}

export default Animal;