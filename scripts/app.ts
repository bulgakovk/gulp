/// <reference path="../typings/angularjs/angular.d.ts" />
import * as angular from 'angular';

// Creates a new module called 'calculatorApp'.
angular.module('testApp', []);

// Registers a controller to our module 'calculatorApp'.
angular.module('testApp').controller('CalculatorController', function CalculatorController($scope) {
    $scope.z = 0;
    $scope.sum = function() {
        $scope.z = $scope.x + $scope.y;
    };
});

//To check async/await
async function test () {
    var a = await test2();
    alert(a);
}

async function test2 () {
    return new Promise(function (resolve, reject) {
        setTimeout( () => {
           resolve(500);
        }, 5000);
    });
}

test();

